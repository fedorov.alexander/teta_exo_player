package com.example.teta.exoplayer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.teta.ExoPlayer.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}